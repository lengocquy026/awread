import { Injectable } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { LoginComponent } from '../login/login.component';
import { RegisterComponent } from '../register/register.component';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public dialog: MatDialog) { }

  openLogIn(): void {
    this.dialog.open(LoginComponent, {
      width: '600px',
      height: '600px'
    });
  }

  openRegister(): void {
    this.dialog.open(RegisterComponent, {
      width: '600px',
      height: '750px'
    });
  }
}
